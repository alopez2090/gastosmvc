﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GASTOSMVC.Models
{
    public class UtilFecha
    {
        public string FNDevFecha(string WDia, string WMes, string WYear)
        {
            DataLayer oRuta = new DataLayer();
            string WRuta = HttpContext.Current.Server.MapPath(oRuta.RUTAFILEDB);
            oRuta.Dispose(); oRuta = null;
            int nEsServer = 0; //int.Parse(FNXML(WRuta, "EsSERVER"));
            string WFecha = "";
            if (nEsServer == 0)
            { //LOCAL
                WFecha = WYear + "-" + WDia.Substring(WDia.Length - 2, 2) + "-" + WMes.Substring(WMes.Length - 2, 2);
            }
            else
            { //SERVER
                WFecha = WYear + "/" + WMes.Substring(WMes.Length - 2, 2) + "/" + WDia.Substring(WDia.Length - 2, 2);
            }
            return WFecha;
        }
    }
}